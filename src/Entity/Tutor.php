<?php

namespace App\Entity;

use App\Repository\TutorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=TutorRepository::class)
 */
class Tutor extends User Implements UserInterface
{

    /**
     * @ORM\Column(type="integer")
     * @Assert\Length(
     *     min = 8,
     *     max = 8,
     *      minMessage = "le num de CIN doit faire {{ limit }} chiffres",
     *      maxMessage = "le num de CIN doit faire {{ limit }} chiffres"
     * )
     */
    private $cin;

    /**
     * @ORM\OneToMany(targetEntity=Student::class, mappedBy="tutor")
     */
    private $students;

    public function __construct()
    {
        $this->students = new ArrayCollection();
    }


    //***********************************************************************************************************
    /**
     * @return mixed
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * @param mixed $cin
     */
    public function setCin($cin): void
    {
        $this->cin = $cin;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->setTutor($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->students->removeElement($student)) {
            // set the owning side to null (unless already changed)
            if ($student->getTutor() === $this) {
                $student->setTutor(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
       return '#'.$this->getId().' '.$this->getFirstName().' '.$this->getLastName();
    }


}
