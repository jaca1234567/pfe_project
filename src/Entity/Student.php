<?php

namespace App\Entity;

use App\Repository\StudentRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * @ORM\Entity(repositoryClass=StudentRepository::class)
 */
class Student extends User implements UserInterface
{
    /**
     * @ORM\ManyToOne(targetEntity=Tutor::class, inversedBy="students")
     * @ORM\JoinColumn(nullable=true)
     */
    private $tutor;

    /**
     * @return Tutor|null
     */
    public function getTutor(): ?Tutor
    {
        return $this->tutor;
    }

    /**
     * @param Tutor|null $tutor
     * @return $this
     */
    public function setTutor(?Tutor $tutor): self
    {
        $this->tutor = $tutor;

        return $this;
    }

    /**
     * @return Tutor|null
     */
    public function getTutorEmail(): ?string
    {
        if (isset($this)) {
            return $this->tutor->getEmail();
        }
    }

}
