<?php

namespace App\Entity;

use App\Repository\ProfessorRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProfessorRepository::class)
 */
class Professor extends User implements UserInterface
{

    /**
     * @ORM\Column(type="integer")
     * @Assert\Length(
     *     min = 8,
     *     max = 8,
     *      minMessage = "le num de CIN doit faire {{ limit }} chiffres",
     *      maxMessage = "le num de CIN doit faire {{ limit }} chiffres"
     * )
     */
    private $cin;

    /**
     * @return mixed
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * @param mixed $cin
     */
    public function setCin($cin): void
    {
        $this->cin = $cin;
    }


}
