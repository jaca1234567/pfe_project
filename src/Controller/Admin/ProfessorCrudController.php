<?php

namespace App\Controller\Admin;

use App\Entity\Professor;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProfessorCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Professor::class;
    }
    public function configureActions(Actions $actions): Actions
    {
        $detailUser=Action::new('detailUser','Detail','fa fa-eye')
            ->linkToCrudAction(Crud::PAGE_DETAIL)
            ->addCssClass('btn btn-outline-primary');




        return $actions
            ->setPermission(Action::DELETE,'ROLE_ADMIN')
            # ->disable(Action::EDIT)
            ->add(Crud::PAGE_INDEX,$detailUser)
            ->update(crud::PAGE_INDEX,Action::NEW,function(Action $action){
                return $action->setIcon('fa fa-user')->addCssClass('btn btn-warning');
            })
            ->update(crud::PAGE_INDEX,Action::EDIT,function(Action $action){
                return $action->setIcon('fa fa-edit')->addCssClass('btn btn-outline-success');
            })
            ->update(crud::PAGE_INDEX,Action::DELETE,function(Action $action) {
                return $action->setIcon('fa fa-trash')->addCssClass('btn btn-outline-danger');
            });

    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('userName');
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')
                ->onlyOnIndex(),
            TextField::new('userName'),
            TextField::new('password')
                ->onlyWhenUpdating(),
            TextField::new('firstName'),
            TextField::new('lastName')
                ->hideOnIndex(),
            IntegerField::new('cin'),
            DateField::new('birthday')
                ->hideOnIndex(),
            TextField::new('gender'),
            TextField::new('address')
                ->hideOnIndex(),
            TelephoneField::new('tel'),
            EmailField::new('email'),
//            ImageField::new('photoFile')
//                ->setFormType(VichImageType::class)
//                ->setLabel('Photo')
        ];
    }


}
