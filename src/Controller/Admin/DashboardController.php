<?php

namespace App\Controller\Admin;

use App\Entity\Professor;
use App\Entity\HygieneOfficer;
use App\Entity\Student;
use App\Entity\Tutor;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="adminDashboard")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function index(): Response
    {
        return $this->render('bundles/EasyAdminBundle/welcome.html.twig',[
            'user'=>[]
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            // the name visible to end users
            ->setTitle('Pascal School')
            // you can include HTML contents too (e.g. to link to an image)
            //->setTitle('<img src="..."> ACME <span class="text-small">Corp.</span>')

            ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('utilisateurs', 'fa fa-users',User::class);
        yield MenuItem::linkToCrud('Elèves', 'fa fa-users', Student::class);
        yield MenuItem::linkToCrud('Tuteurs', 'fa fa-users', Tutor::class);
        yield MenuItem::linkToCrud('Enseignants', 'fa fa-users',Professor::class);
        yield MenuItem::linkToCrud("Agents d'hygiène", 'fa fa-users',HygieneOfficer::class);
    }

}
