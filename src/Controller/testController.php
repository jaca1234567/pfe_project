<?php


namespace App\Controller;


use App\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class testController extends AbstractController
{
    /**
     * @Route("/test","test")
     */
    public function index() :Response
    {
        return $this->render('base2.html.twig');
    }


}