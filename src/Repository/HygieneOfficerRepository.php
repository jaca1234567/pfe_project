<?php

namespace App\Repository;

use App\Entity\HygieneOfficer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HygieneOfficer|null find($id, $lockMode = null, $lockVersion = null)
 * @method HygieneOfficer|null findOneBy(array $criteria, array $orderBy = null)
 * @method HygieneOfficer[]    findAll()
 * @method HygieneOfficer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HygieneOfficerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HygieneOfficer::class);
    }

    // /**
    //  * @return HygieneOfficer[] Returns an array of HygieneOfficer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HygieneOfficer
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
