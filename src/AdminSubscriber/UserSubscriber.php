<?php


namespace App\AdminSubscriber;

use App\Entity\HygieneOfficer;
use App\Entity\Professor;
use App\Entity\Student;
use App\Entity\Tutor;
use App\Services\PasswordGenerator;
use App\Services\SwiftMailerService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use App\Entity\Contact;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class UserSubscriber implements EventSubscriberInterface
{
    const TEMPLATE_CONTACT = "email/contact.html.twig";

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var PasswordGenerator
     */
    protected PasswordGenerator $passwordGenerator;

    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * @var SwiftMailerService
     */
    private SwiftMailerService $mailerService;


    public function __construct(EntityManagerInterface $entityManager, PasswordGenerator $passwordGenerator, UserPasswordEncoderInterface $passwordEncoder, SwiftMailerService $mailerService)
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->mailerService = $mailerService;
        $this->passwordGenerator = $passwordGenerator;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['addUser'],
            //BeforeEntityUpdatedEvent::class => ['updateUser'], //surtout utile lors d'un reset de mot passe plutôt qu'un réel update, car l'update va de nouveau encrypter le mot de passe DEJA encrypté ...
        ];
    }
//    public function updateUser(BeforeEntityUpdatedEvent $event)
//    {
//        $entity = $event->getEntityInstance();
//
//        if ($entity instanceof User) {
//            // ************* reset password !!!!!!!!!!* *****************************
//            //******** send mail to the user containing the plain password  ***********
//            $contact = $this->createContact($entity, $plainPwd);
//            try {
//                $this->sendContact($contact);
//            } catch (LoaderError | RuntimeError | SyntaxError $e) {
//            }
//
//            //******** encrypt the plain password ($plainPwd) and persist the User entity in DB *****
//            $this->cryptPassword($entity, $plainPwd);
//     }
//    }

    /**
     * @param BeforeEntityPersistedEvent $event
     */
    public function addUser(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if ($entity instanceof User) {
            //********  generate random password  *************************************
            $plainPwd = $this->passwordGenerator->generateRandomStrongPassword(8);

                //******* Student mail ***********
            if($entity instanceof Student){
                //set student email (= the tutor's one)
                $tutorMail = $entity->getTutorEmail();
                $entity->setEmail($tutorMail);
            }
                //*******/. Student mail

            //******** send mail to the user containing the plain password  ***********
            $contact = $this->createContact($entity, $plainPwd);
            try {
                $this->sendContact($contact);
            } catch (LoaderError | RuntimeError | SyntaxError $e) {
            }

            //******** encrypt the plain password ($plainPwd) and persist the User entity in DB *****
            $this->cryptPassword($entity, $plainPwd);
            //******** set roles*******************************************************************
//            $s = array('ROLE_STUDENT');
//            $t = array('ROLE_TUTOR');
//            $p = array('ROLE_PROFESSOR');
//            $h = array('ROLE_HYGIENE-OFFICER');
//
//            if ($entity instanceof Student){
//                $entity->setRoles($s);
//            }
//            if ($entity instanceof Tutor){
//                $entity->setRoles($t);
//            }
//            if ($entity instanceof Professor){
//                $entity->setRoles($p);
//            }
//            if ($entity instanceof HygieneOfficer){
//                $entity->setRoles($h);
//            }
        }
    }

    /**
     * @param User $entity
     * @param string $plainPwd
     * @return Contact
     */
    public function createContact(User $entity,string $plainPwd): Contact {

        //get data
        $name=$entity->getFirstName();
        $email=$entity->getEmail();
        $message= "Votre mot de passe est : ".$plainPwd ;

        //create Contact entity
        $contact= new Contact;
        $contact->setName($name);
        $contact->setEmail($email);
        $contact->setDescription($message);
        return $contact;

    }
    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sendContact(Contact $contact)
    {
        $TEMPLATE_CONTACT = "email/contact.html.twig";
        $parameters = [
            "email" => $contact->getEmail(),
            "name" => $contact->getName(),
            "description" => $contact->getDescription()
        ];

        $this->mailerService->send(
            "Pascal school, mot de passe de votre compte",
            ['pascal.Primary.School@gmail.com'],
            [$contact->getEmail()],
            $TEMPLATE_CONTACT,
            $parameters
        );
    }


    /**
     * @param User $entity
     * @param string $plainPwd
     */
    public function cryptPassword(User $entity,string $plainPwd): void
    {
        $entity->setPassword(
            $this->passwordEncoder->encodePassword(
                $entity,
                $plainPwd
            )
        );
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }


}